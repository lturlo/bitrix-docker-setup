#!/bin/bash
wget -qO- https://www.1c-bitrix.ru/download/start_encode.tar.gz | tar xvz --directory ./app

docker-compose up -d --build

docker-compose exec php71 mkdir /srv/www/tmp
docker-compose exec php71 chmod -R 755 /srv/www
