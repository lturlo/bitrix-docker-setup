FROM php:7.1.3-fpm

RUN apt-get update && apt-get install -y mysql-client libfreetype6-dev \
        libicu-dev \
        libmcrypt-dev \
        libpng12-dev \
        libjpeg-dev \
        libpng-dev \
        zlib1g-dev \
        zip 

RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-install -j$(nproc) intl

RUN docker-php-ext-install pdo pdo_mysql \
    && docker-php-ext-install session \
    && docker-php-ext-install mysqli \
    && docker-php-ext-install zip
